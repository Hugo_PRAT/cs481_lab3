import 'package:flutter/material.dart';

import 'package:flutter/cupertino.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      darkTheme: ThemeData.dark(),
      theme: ThemeData(brightness: Brightness.dark),
      title: 'CS 81',
      home: new MyHomePage(title: 'LAB 3'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final List<ActorFilterEntry> _member = <ActorFilterEntry>[
    const ActorFilterEntry('Hugo Prat', 'HP'),
    const ActorFilterEntry('Daniel Martinez', 'DM'),
    const ActorFilterEntry('Ralph Lira', 'RL'),
    const ActorFilterEntry('Hugo Janasik', 'HJ'),
    const ActorFilterEntry('Chihiro Nishijima', 'CN'),
  ];
  List<String> _selected = <String>[];
  List<String> _options = ["Hugo", "Daniel", "Ralph", "Chihiro"];
  String name = "Hugo";
  bool visible = false;

  bool _wifi = false;
  bool _bluetooth = false;

  Iterable<Widget> get groupWidget sync* {
    for (ActorFilterEntry actor in _member) {
      yield Padding(
        padding: const EdgeInsets.all(4.0),
        child: FilterChip(
          avatar: CircleAvatar(child: Text(actor.initials)),
          label: Text(actor.name),
          selected: _selected.contains(actor.name),
          onSelected: (bool value) {
            setState(() {
              if (value) {
                _selected.add(actor.name);
              } else {
                _selected.removeWhere((String name) {
                  return name == actor.name;
                });
              }
            });
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[

            ActionChip(
              label: Text('Click me'),
              labelStyle: TextStyle(color: Colors.black),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (_) => AlertDialog(
                        title: Text("Get clicked"),
                        content: Text("Goodjob"),
                ));
              },
              backgroundColor: Colors.yellowAccent,
              ),

            Wrap(
              children: [
                InputChip(
                  avatar: Icon(Icons.wifi),
                  label: Text('Wifi'),
                  labelStyle: TextStyle(
                      fontSize: 20,
                      color: _wifi ? Colors.black : Colors.white),
                  padding: EdgeInsets.all(16),
                  pressElevation: 25,
                  onSelected: (isSelected) {
                    setState(() {
                      _wifi = !_wifi;
                    });
                  },
                  selected: _wifi,
                  selectedColor: Colors.redAccent,
                  checkmarkColor: Theme.of(context).accentColor,
                ),
                InputChip(
                  avatar: Icon(Icons.bluetooth),
                  label: Text('Bluetooth'),
                  labelStyle: TextStyle(
                      fontSize: 20,
                      color: _bluetooth ? Colors.black : Colors.white),
                  padding: EdgeInsets.all(16),
                  pressElevation: 25,
                  onSelected: (isSelected) {
                    setState(() {
                      _bluetooth = !_bluetooth;
                    });
                  },
                  selected: _bluetooth,
                  selectedColor: Colors.blueAccent,
                  checkmarkColor: Theme.of(context).accentColor,
                ),
              ],
            ),

            Chip(
              avatar:
                  _wifi ? CircularProgressIndicator() : null,
              label: Text(
                  '${_wifi ? 'Downloading...' : 'Download'}'),
              labelStyle: TextStyle(color: Colors.white),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Wrap(
                  children: groupWidget.toList(),
                ),
                Text('${_selected.join(', ')}'),
              ],
            ),
            DropdownButton(
              value: name,
              icon: Icon(Icons.arrow_downward, color: Colors.grey,),
              underline: Container(
                height: 2,
                width: 4,
                color: Colors.grey,
              ),
              onChanged: (String value) {
                setState(() {
                  name = value;
                });
              },
              items: _options.map((String value) {
                return DropdownMenuItem<String>(child: Text(value), value: value,);
              } ).toList(),
            ),
            SizedBox(height: 20,),
            Container(
              width: 200,
              child: Card( child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.person),
                    title: Text(name),
                    subtitle: Text('A developer master.'),
                  ),
                  FlatButton(
                    child: Text('Give all the points'),
                    onPressed: () {print("good choice");},
                  ),],
              ),))
          ],
        ),
      ),
    );
  }
}

class ActorFilterEntry {
  const ActorFilterEntry(this.name, this.initials);
  final String name;
  final String initials;
}